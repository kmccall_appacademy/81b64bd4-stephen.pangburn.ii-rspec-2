# performance_monitor.rb

def measure(reps = 1)
  benchmarks = []

  reps.times do 
    start = Time.now
    yield
    benchmarks << Time.now - start
  end

  benchmarks.reduce(:+) / benchmarks.length
end
