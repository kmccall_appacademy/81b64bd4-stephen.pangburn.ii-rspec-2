# silly_blocks.rb

def reverser
  string = yield
  string.split.map(&:reverse).join(" ")
end

def adder(operand = 1)
  yield + operand
end

def repeater(reps = 1)
  reps.times { yield }
end
